#include <iostream>
using namespace std;


const int row = 6;
const int column = 7;
int playerStatus = 0;
int currentRow, currentColumn;

void displayGrid(int grid[][column]); //prints out the grid
void input(int grid[][column],int&, int&); //get input from user
void horizontalCheck(int grid[][column],int,int); //check stack horizontally
void verticalCheck(int grid[][column],int,int); //check stack vertically
void leftDiagonalCheck(int grid[][column],int,int,int); //Check stack left diagnally
void rightDiagonalCheck(int grid[][column], int, int, int); //check stack right diagnally
void noMoveCheck(int grid[][column]);//Check if it no moves

int main() {
	int grid[row][column] = { 0 };
	int userSwitch = 1, turn = 1;
	bool loop = true;

	while (loop == true) {
		//system("cls");
		cout << "user #" << userSwitch << " turn " << turn << endl;
		displayGrid(grid);
		horizontalCheck(grid, userSwitch, currentRow);
		verticalCheck(grid, userSwitch, currentColumn);
		leftDiagonalCheck(grid, userSwitch, currentRow, currentColumn);
		rightDiagonalCheck(grid, userSwitch, currentRow, currentColumn);
		input(grid,userSwitch, turn);
		noMoveCheck(grid); 
		if (playerStatus != 0) { //if end game it will exit loops
			break;
		}
	}
	return 0;
}
void displayGrid(int grid[][column]){

	for (int i = 0; i < row; i++) {
		for (int j = 0; j < column; j++) {
			cout << grid[i][j] << " ";
		}
		cout << endl;
	}
}
void input(int grid[][column],int& userSwitch, int& turn) {

	int input;
	bool loop = true;
	//displayGrid(grid);
	//cout << endl;
	cin >> input;
	input = input - 1; //convert to array number

	if (input >= 0 && input <= 6) { //input must be in 1 - 7
		if (grid[0][input] == 0) { //Check if row isn't empty.
			for (int i = row; i >= 0; i--) { //Pushes back to buttom.
				if (grid[i][input] == 0) {
					//cout << "passed";
					grid[i][input] = userSwitch;
						
					currentRow = i;
					currentColumn = input;

					//cout << currentRow << " " << currentColumn << endl;
					turn++;


					if (userSwitch == 1) {
						userSwitch = 2;
					}
					else if (userSwitch == 2) {
						userSwitch = 1;
					}
					break;
				}
			}
		}

		else {
			cout << "this row isn't empty" << endl;
			loop = true;
		}
	} 
	else{ //if input isn't 1-6
		cout << "invalid input" << endl;
		loop = true;
	}
	cout << endl;
}

void horizontalCheck(int grid[][column], int currentUser, int currentRow) {
	//for (int i = 0; i < row; i++) {
		int point = 0;
		for (int j = 0; j < column; j++) {
			if (grid[currentRow][j] == currentUser) {
				point++;
				if(point==4)
				{
					displayGrid(grid);
					cout << "Player #" << currentUser << " wins!" << endl;
					cout << "Horizontally" << endl;
					playerStatus = currentUser;
				}
			}
			else {
				//cout << "passed hz"<<endl;
				point = 0;
			}
		}
	//}
}

void verticalCheck(int grid[][column], int currentUser,int currentColumn) {

	int point = 0;
	for (int j = 0; j < row; j++) {
		if (grid[j][currentColumn] == currentUser) {
			point++;
			if (point == 4)
			{
				displayGrid(grid);
				cout << "Player #" << currentUser <<  " wins!" << endl;
				cout << "Vertically" << endl;
				playerStatus = currentUser;
			}
		}
		else {
			//cout << "passed vc"<<endl;
			point = 0;
		}
	}
}

void leftDiagonalCheck(int grid[][column], int currentUser, int currentRow ,int currentColumn) {
	int point= 0,pointright=0;
	//to get the current row / column and see near cell if it current user it will +point
	if (grid[currentRow][currentColumn] == currentUser) {
		point=1;
		for (int i = 1; i <= 3; i++) {
			if (grid[currentRow - i][currentColumn - i] == currentUser && (currentRow - i)>= 0 && (currentColumn - i) >= 0) {
				//cout << "check-" << endl;
				point++;
			}
			if (grid[currentRow +i][currentColumn +i] == currentUser && (currentRow + i) <= 6 && (currentColumn + i) <= 7) {
				//cout << "check+" << endl;
				point++;
				
			}
		}

		if (point == 4) { //connect 4!
			displayGrid(grid);
			cout << "Player #" << currentUser << " wins!" << endl;
			cout << "Diagonally left " << endl;
			playerStatus = currentUser;
		}

		point = 0;
		cout << point;
	}
}
	

void rightDiagonalCheck(int grid[][column], int currentUser, int currentRow, int currentColumn) {
	int point = 0, pointright = 0;
	//to get the current row / column and see near cell if it current user it will +point
	if (grid[currentRow][currentColumn] == currentUser) {
		point=1;
		for (int i = 1; i <= 3; i++) {
			if (grid[currentRow - i][currentColumn + i] == currentUser && (currentRow - i) >= 0 && (currentColumn + i) <=7) {
				//cout << "check-" << endl;
				point++;
			}
			if (grid[currentRow + i][currentColumn - i] == currentUser && (currentRow + i) <= 6 && (currentColumn - i) >=0) {
				point++;
				//cout << "check+" << endl;
			}
		}

		if (point == 4) { //connect 4!
			displayGrid(grid);
			cout << "Player #" << currentUser << " wins!" << endl;
			cout << "Diagonally right " << endl;
			playerStatus = currentUser;
		}
		point = 0;
		//cout << point;
	}
}

void noMoveCheck(int grid[][column]) {
	int enteredCell = 0;
	for (int i = 0; i < row; i++) {
		for (int j = 0; j < column; j++) {
			if (grid[i][j] != 0) {
				enteredCell++;
			}
		}
	}
	if (enteredCell == 42) { //all cells are fulled
		cout << "No Moves!!!!!" << endl;
		playerStatus = 5;
	}
}
